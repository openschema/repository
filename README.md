# Repository


Path for schema files should be organised by following path logic. At first the schema context, then the version and then the files with language dependent iso code suffix.

```
repository/
    {context}/
        {version}/
            schema.json    # <-- default schema (in english)
            schema.de.json # <- german translation
    exmaple/
        10.0/
            person.json    
            user.json    

```
